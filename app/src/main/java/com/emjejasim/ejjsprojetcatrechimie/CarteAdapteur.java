package com.emjejasim.ejjsprojetcatrechimie;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class CarteAdapteur extends ArrayAdapter<Carte> {
    private Context context;
    private LayoutInflater inflater;
    private String currentImage = "recto";
    private String mecanisme = "non";
    private DisplayMetrics displayMetrics;
    int heightPixels;

    public CarteAdapteur(@NonNull Context context, ArrayList<Carte> tabCartes) {
        super(context, 0, tabCartes);
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    public View getView(int position, View view, ViewGroup parent) {
        if(view == null){
            // Permet d'utiliser le layout personnalisé dans la listview
            view = inflater.inflate(R.layout.layout_carte, parent, false);
        }

        displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

        DisplayMetrics metrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(metrics);

        heightPixels = metrics.heightPixels;



        ImageView imageViewCarte = view.findViewById(R.id.imgCarte);
        imageViewCarte.getLayoutParams().height = (int) (heightPixels * 0.3);
        Button boutonRep = view.findViewById(R.id.btnMecanisme);

        // TODO Récupérer les objets du layout personnalis
        Carte carte = this.getItem(position);

        imageViewCarte.setImageDrawable(context.getDrawable(carte.imageRecto));
        Log.d("testimage", String.valueOf(carte.imageRecto));

        imageViewCarte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageViewCarte.getLayoutParams().height = (int) (heightPixels * 0.3);
                if (mecanisme.equals("oui")){
                    mecanisme = "non";
                    if (currentImage.equals("verso")){
                        imageViewCarte.setImageDrawable(context.getDrawable(carte.imageVerso));
                    }
                    else if (currentImage.equals("recto")){
                        imageViewCarte.setImageDrawable(context.getDrawable(carte.imageRecto));
                    }
                }
                else{
                    if (currentImage.equals("recto")){
                        imageViewCarte.setImageDrawable(context.getDrawable(carte.imageVerso));
                        currentImage = "verso";
                    }
                    else if (currentImage.equals("verso")){
                        imageViewCarte.setImageDrawable(context.getDrawable(carte.imageRecto));
                        currentImage = "recto";
                    }
                }

            }
        });

        boutonRep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mecanisme.equals("non")){
                    imageViewCarte.setImageDrawable(context.getDrawable(carte.imageMecanisme));
                    mecanisme= "oui";
                    imageViewCarte.getLayoutParams().height = (int) (heightPixels * 0.8);
                }
                else if (mecanisme.equals("oui")){
                    mecanisme = "non";
                    imageViewCarte.getLayoutParams().height = (int) (heightPixels * 0.3);
                    if (currentImage.equals("verso")){
                        imageViewCarte.setImageDrawable(context.getDrawable(carte.imageVerso));
                    }
                    else if (currentImage.equals("recto")){
                        imageViewCarte.setImageDrawable(context.getDrawable(carte.imageRecto));
                    }
                }
            }

        });

        // TODO Donner une valeur aux objets du layout personnalisé
        return view;
    }
}
