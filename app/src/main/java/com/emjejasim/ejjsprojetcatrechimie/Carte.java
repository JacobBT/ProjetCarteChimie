package com.emjejasim.ejjsprojetcatrechimie;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class Carte {
    int id;
    ArrayList<String> cat;
    int imageRecto;
    int imageVerso;
    int imageMecanisme;



    /**
     * Constructeur de la classe Cartes
     * @param cat Type de la molécule
     * @param imageRecto Image du recto de la carte
     * @param imageVerso Image du verso de la carte
     * @param imageMecanisme Image du mécanisme de la carte
     */
    public Carte(int id, ArrayList<String> cat, int imageRecto, int imageVerso, int imageMecanisme){
        this.id = id;
        this.cat = cat;
        this.imageRecto = imageRecto;
        this.imageVerso = imageVerso;
        this.imageMecanisme = imageMecanisme;
    }




}
