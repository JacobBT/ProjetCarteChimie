package com.emjejasim.ejjsprojetcatrechimie;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    ListView lsView;
    ArrayList<Carte> lsCartes;
    CarteAdapteur adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lsView = findViewById(R.id.lsView);

        lsCartes = convertJsonToCartes(MainActivity.this);

        adapter = new CarteAdapteur(this, lsCartes);

        lsView.setAdapter(adapter);
    }


    /**
     * Fonction convertissant les données du JSOn en string
     * @param context context
     * @return
     */
    public String loadJSONFromResource(Context context) {
        String jsonString;
        try {
            InputStream is = context.getResources().openRawResource(R.raw.donnees);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            jsonString = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return jsonString;
    }

    /**
     * Fonction récupérant les objets du fichier JSON et les converti en carte
     * Générée par ChatGpt, puis adaptée par Jacob
     * @param context Context
     * @return la ArrayList des cartes récupérées
     */
    public ArrayList<Carte> convertJsonToCartes(Context context) {
        ArrayList<Carte> cartes = new ArrayList<Carte>();
        String jsonString = loadJSONFromResource(context);
        try {
            JSONObject jsnObjct = new JSONObject(jsonString);
            JSONArray jsonArray = jsnObjct.getJSONArray("rx");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt("id");
                JSONArray catArray = jsonObject.getJSONArray("cat");
                ArrayList<String> catList = new ArrayList<>();
                for (int j = 0; j < catArray.length(); j++) {
                    catList.add(catArray.getString(j));
                }
                String imageRectoNom = jsonObject.getString("q");
                imageRectoNom = imageRectoNom.replaceAll("\\.[^.]+$", "").toLowerCase(); //Code
                int imageRecto = getResId(imageRectoNom, R.drawable.class);
                String imageVersoNom = jsonObject.getString("r");
                imageVersoNom = imageVersoNom.replaceAll("\\.[^.]+$", "").toLowerCase();
                int imageVerso =  getResId(imageVersoNom, R.drawable.class);
                String imageMecanismeNom = jsonObject.getString("e");
                imageMecanismeNom = imageMecanismeNom.replaceAll("\\.[^.]+$", "").toLowerCase();
                int imageMecanisme = getResId(imageMecanismeNom, R.drawable.class);
                Carte carte = new Carte(id, catList, imageRecto, imageVerso, imageMecanisme);
                cartes.add(carte);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cartes;
    }

    /**
     * Fonction récupérant le nom d'un fichier ressource, sans extension, puis de quel classe vient-il et donne son id
     * Résupérée de StackOverflow de la réponse de Macarse et OneCricketer au billet suivant:
     * https://stackoverflow.com/questions/4427608/android-getting-resource-id-from-string
     * @param resName Le nom de la ressource à trouver l'id
     * @param c La classe où se trouve la ressource (ex. R.drawable.class)
     * @return Le id de la ressource
     */
    public static int getResId(String resName, Class<?> c) {

        try {
            Field idField = c.getDeclaredField(resName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

}